# Zoom in a web page using CSS 3D

Zoom in a web page by clicking anywhere.

It uses CSS 3D to scale and move the main element to create a zoom behavior.

# How to test

Clone the repository.

Open index.html in your favorite browser.

# How to use

Click to zoom to desired area.

Click again to return to original state.
